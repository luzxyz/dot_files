call plug#begin('~/.config/nvim/plugged')
	"Some color themes
        Plug 'cocopon/iceberg.vim'
	Plug 'AlessandroYorba/Alduin'

	"Web development plugins
	Plug 'mattn/emmet-vim'
	Plug 'tpope/vim-surround'
	Plug 'alvan/vim-closetag'
	Plug 'neoclide/coc.nvim', {'branch': 'release'}

	"Commentaries // Needs configuration for certain file extensions
	Plug 'tpope/vim-commentary'

	"Tag plugins
	Plug 'neoclide/coc-pairs'
	Plug 'jiangmiao/auto-pairs'
	Plug 'vim-scripts/c.vim', {'for': ['c', 'cpp']}

	"Navigation
"	Plug 'preservim/nerdtree'
"	Plug 'Xuyuanp/nerdtree-git-plugin'

	"rust development
	Plug 'rust-lang/rust.vim'		
call plug#end()

syntax enable
filetype plugin indent on

set t_Co=256
colorscheme alduin
set relativenumber

"Plugin Configurations

" Start NERDTree. If a file is specified, move the cursor to its window.
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif

" Exit Vim if NERDTree is the only window remaining in the only tab.
"autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif


"Only satan knows about these...

"hi Normal guibg=NONE ctermbg=NONE 
"set background=light
"let g:alduin_Shout_Dragon_Aspect = 1
"highlight Normal ctermbg=NONE
"highlight nonText ctermbg=NONE
