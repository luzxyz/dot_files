#>>>>>>>>>>>>>>>>>>
# Set up the prompt
#>>>>>>>>>>>>>>>>>>

# Old default propmts
#autoload -Uz promptinit
#promptinit
#prompt adam1
#setopt histignorealldups sharehistory

autoload -Uz vcs_info

# Git information output
precmd() { vcs_info }
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git:*' formats '<%s:%b(%u%c)>'


setopt PROMPT_SUBST


#RPROMPT='{%/}[${vcs_info_msg_0_}]'
RPROMPT='%U%F{cyan}[${vcs_info_msg_0_}]%f%u'
#PROMPT='>'
PROMPT='%S%F{green}   %* %# %F{yellow}%2d%f%F{blue} %j %? %s┫%f%f
%F{green}┗ %f'


# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Autocompletion for zsh
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# PLUGIN: fzf-tab
source ~/zshplugins/fzf-tab/fzf-tab.plugin.zsh

# Add ~bin to path
PATH="$HOME/bin:$PATH"

# Custom aliases
#alias ls='ls -lh --color=auto'
#alias guixmp='. ~/Documents/XAMPP.sh'
alias gimmehour="date +'%T'"
#alias showoff='neofetch | lolcat'
alias image='sxiv'
alias leer='apvlv'
#alias IRC='irssi'
alias fzf='fzf --preview="less {}"'
alias lsblk='lsblk -pT -o NAME,FSTYPE,UUID,MOUNTPOINT,SIZE'
#alias free='free -gh'
alias ps='ps -A --sort -rss -o comm,pmem,rss,pid'
alias xbepis='xbps-query -l | wc -l'
alias xpepis='xbps-query -m | wc -l'
alias v='nvim'
alias cp='cp -v'
alias exa='exa -lh'
alias exg='exa --tree --git'
alias xbi="doas xbps-install"
alias xbq="xbps-query"
alias xbr="doas xbps-remove"
alias xbu="doas xbps-install -Su"
alias news="newsboat"

# Start, end, supr keys 
bindkey  "^[[7~"   beginning-of-line
bindkey  "^[[8~"   end-of-line
bindkey  "^[[3~"  delete-char
