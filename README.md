# configuration files for utils on my build of Linux

I organize the files as they appear in the home directory (~)

If a dotfile is archived here it doesnt mean that i'm actively using it, so use this repository
and then filter what you need.

These dotfiles may have MIT or GPLv# Licences, as i'm lazy i wont search for them.

these files on this repos are GPLv2 licensed.

_OTHERS contains miscellanous scrips and files, there's no need on having it on the home directory
